/* 
 * SPLPv1.c
 * The file is part of practical task for System programming course. 
 * This file contains validation of SPLPv1 protocol. 
 */


/*
  Януш Геман Сергеевич
  14 группа
*/

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "splpv1.h"

/*
---------------------------------------------------------------------------------------------------------------------------
# |      STATE      |         DESCRIPTION       |           ALLOWED MESSAGES            | NEW STATE | EXAMPLE
--+-----------------+---------------------------+---------------------------------------+-----------+----------------------
1 | INIT            | initial state             | A->B     CONNECT                      |     2     |
--+-----------------+---------------------------+---------------------------------------+-----------+----------------------
2 | CONNECTING      | client is waiting for con-| A<-B     CONNECT_OK                   |     3     |
  |                 | nection approval from srv |                                       |           |                      
--+-----------------+---------------------------+---------------------------------------+-----------+----------------------
3 | CONNECTED       | Connection is established | A->B     GET_VER                      |     4     |                     
  |                 |                           |        -------------------------------+-----------+----------------------
  |                 |                           |          One of the following:        |     5     |                      
  |                 |                           |          - GET_DATA                   |           |                      
  |                 |                           |          - GET_FILE                   |           |                      
  |                 |                           |          - GET_COMMAND                |           |
  |                 |                           |        -------------------------------+-----------+----------------------
  |                 |                           |          GET_B64                      |     6     |                      
  |                 |                           |        ------------------------------------------------------------------
  |                 |                           |          DISCONNECT                   |     7     |                                 
--+-----------------+---------------------------+---------------------------------------+-----------+----------------------
4 | WAITING_VER     | Client is waiting for     | A<-B     VERSION ver                  |     3     | VERSION 2                     
  |                 | server to provide version |          Where ver is an integer (>0) |           |                      
  |                 | information               |          value. Only a single space   |           |                      
  |                 |                           |          is allowed in the message    |           |                      
--+-----------------+---------------------------+---------------------------------------+-----------+----------------------
5 | WAITING_DATA    | Client is waiting for a   | A<-B     CMD data CMD                 |     3     | GET_DATA a GET_DATA 
  |                 | response from server      |                                       |           |                      
  |                 |                           |          CMD - command sent by the    |           |                      
  |                 |                           |           client in previous message  |           |                      
  |                 |                           |          data - string which contains |           |                      
  |                 |                           |           the following allowed cha-  |           |                      
  |                 |                           |           racters: small latin letter,|           |                     
  |                 |                           |           digits and '.'              |           |                      
--+-----------------+---------------------------+---------------------------------------+-----------+----------------------
6 | WAITING_B64_DATA| Client is waiting for a   | A<-B     B64: data                    |     3     | B64: SGVsbG8=                    
  |                 | response from server.     |          where data is a base64 string|           |                      
  |                 |                           |          only 1 space is allowed      |           |                      
--+-----------------+---------------------------+---------------------------------------+-----------+----------------------
7 | DISCONNECTING   | Client is waiting for     | A<-B     DISCONNECT_OK                |     1     |                      
  |                 | server to close the       |                                       |           |                      
  |                 | connection                |                                       |           |                      
---------------------------------------------------------------------------------------------------------------------------

IN CASE OF INVALID MESSAGE THE STATE SHOULD BE RESET TO 1 (INIT)

*/


enum FW_TRANSITION_RESULT { FW_TRANSITION_SUCCESS, FW_TRANSITION_FAILURE };

enum FW_TRANSITION_RESULT FW_LastTransitionResult;

typedef int (*FW_DataValidationFunc)(char* pData);
FW_DataValidationFunc validationFunc = NULL;

#define FW_MsgVersionPrefixLength 9
const char FW_MsgVersionPrefix[FW_MsgVersionPrefixLength] = "VERSION ";

int FW_ValidateVer(char* pData) {
	if (strncmp(FW_MsgVersionPrefix, pData, FW_MsgVersionPrefixLength - 1) != 0)
		return 0;
	pData += FW_MsgVersionPrefixLength - 1;
	if (*pData == ' ') return 0;
	char* end;
	long v = strtol(pData, &end, 10);
	if (*end != '\0')
		return 0;
	if (v <= 0)
		return 0;

	return 1;
}

char FW_DataCommandLegalSymbols[128] = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0,
	1, // dot symbol [46]
	0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // digits [48:57]
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // lowercase letters [97:122]
	0, 0, 0, 0, 0
};

char* getFirstIncorrectSymbol(char* pData) {
	char* result = pData;
	while (*result != ' ') {
		if (!FW_DataCommandLegalSymbols[(size_t)*result])
			break;
		++result;
	}
	return result;
}

int FW_ValidateDataCommand(char* pData, const char* prefix, const char* postfix, size_t cmdLength) {
	if (strncmp(prefix, pData, cmdLength - 1) != 0) {
		return 0;
	}
	pData += cmdLength - 1;
	pData = getFirstIncorrectSymbol(pData);
	if (strncmp(postfix, pData, cmdLength) != 0) {
		return 0;
	}
	return 1;
}

#define FW_MsgGetDataLength 10
const char FW_MsgGetDataPrefix[FW_MsgGetDataLength] = "GET_DATA ";
const char FW_MsgGetDataPostfix[FW_MsgGetDataLength] = " GET_DATA";

int FW_ValidateData(char* pData) {
	return FW_ValidateDataCommand(pData, FW_MsgGetDataPrefix, FW_MsgGetDataPostfix, FW_MsgGetDataLength);
}

#define FW_MsgGetFileLength 10
const char FW_MsgGetFilePrefix[FW_MsgGetFileLength] = "GET_FILE ";
const char FW_MsgGetFilePostfix[FW_MsgGetFileLength] = " GET_FILE";

int FW_ValidateFile(char* pData) {
	return FW_ValidateDataCommand(pData, FW_MsgGetFilePrefix, FW_MsgGetFilePostfix, FW_MsgGetFileLength);
}

#define FW_MsgGetCommandLength 13
const char FW_MsgGetCommandPrefix[FW_MsgGetCommandLength] = "GET_COMMAND ";
const char FW_MsgGetCommandPostfix[FW_MsgGetCommandLength] = " GET_COMMAND";

int FW_ValidateCommand(char* pData) {
	return FW_ValidateDataCommand(pData, FW_MsgGetCommandPrefix, FW_MsgGetCommandPostfix, FW_MsgGetCommandLength);
}

#define FW_MsgB64PrefixLength 6
const char FW_MsgB64Prefix[FW_MsgB64PrefixLength] = "B64: ";

char FW_B64LegalSymbols[128] = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0,
	1, // plus symbol [43]
	0, 0, 0,
	1, // slash symbol [47]
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // digits [48:57]
	0, 0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // uppercase letters [65:90]
	0, 0, 0, 0, 0, 0,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // lowercase letters [97:122]
	0, 0, 0, 0, 0
};

int FW_ValidateB64(char* pData) {
	if (memcmp(FW_MsgB64Prefix, pData, FW_MsgB64PrefixLength - 1) != 0)
		return 0;
	pData += FW_MsgB64PrefixLength - 1;
	// int dataLength = 0;
	char* t = pData;
	while (*t != '\0') {
		if (!FW_B64LegalSymbols[(size_t)*t])
			break;
		++t;
		// ++dataLength;
	}
	if (*t == '=') {
		//++dataLength;
		++t;
		if (*t == '=') {
			//++dataLength;
			++t;
			if (*t != '\0')
				return 0;
		}
		else if (*t != '\0') return 0;
	}
	else if (*t != '\0') return 0;
	if ((t - pData) % 4 != 0) return 0;
	return 1;
}


enum FW_STATE {
	FW_STATE_INIT = 1,
	FW_STATE_CONNECTING = 2,
	FW_STATE_CONNECTED = 3,
	FW_STATE_WAITING_VER = 4,
	FW_STATE_WAITING_DATA = 5,
	FW_STATE_WAITING_B64_DATA = 6,
	FW_STATE_DISCONNECTING = 7
};

enum FW_STATE FW_FallbackState = FW_STATE_INIT;
enum FW_STATE FW_CurrentState = FW_STATE_INIT;

int FW_CheckLegalTransition(enum FW_STATE src, enum FW_STATE dest) {
	switch (src) {
	case FW_STATE_INIT:
		return dest == FW_STATE_CONNECTING;
	case FW_STATE_CONNECTING:
		return dest == FW_STATE_CONNECTED;
	case FW_STATE_CONNECTED:
		return dest == FW_STATE_WAITING_VER || dest == FW_STATE_WAITING_DATA || dest == FW_STATE_WAITING_B64_DATA ||
			dest == FW_STATE_DISCONNECTING;
	case FW_STATE_WAITING_VER:
	case FW_STATE_WAITING_DATA:
	case FW_STATE_WAITING_B64_DATA:
		return dest == FW_STATE_CONNECTED;
	case FW_STATE_DISCONNECTING:
		return dest == FW_STATE_INIT;
	default:
		return 0;
	}
}

void FW_DropToFallbackState() {
	FW_LastTransitionResult = FW_TRANSITION_FAILURE;
	FW_CurrentState = FW_FallbackState;
	validationFunc = NULL;
}

void FW_SwitchState(enum FW_STATE dest, FW_DataValidationFunc validate) {
	if (FW_CheckLegalTransition(FW_CurrentState, dest)) {
		FW_CurrentState = dest;
		validationFunc = validate;
		FW_LastTransitionResult = FW_TRANSITION_SUCCESS;
		return;
	}
	FW_DropToFallbackState();
}

void FW_ProcessArbitraryData(char* pData) {
	if (validationFunc(pData))
		FW_SwitchState(FW_STATE_CONNECTED, NULL);
	else
		FW_DropToFallbackState();
}

int FW_CheckDirection(enum Direction expected, enum Direction actual) {
	if (actual == expected)
		return 1;
	FW_DropToFallbackState();
	return 0;
}

void FW_ProcessMessage(struct Message* msg) {
	if (validationFunc == NULL) {
		if (strcmp(msg->text_message, "CONNECT") == 0) {
			if (FW_CheckDirection(A_TO_B, msg->direction))
				FW_SwitchState(FW_STATE_CONNECTING, NULL);
		}
		else if (strcmp(msg->text_message, "CONNECT_OK") == 0) {
			if (FW_CheckDirection(B_TO_A, msg->direction))
				FW_SwitchState(FW_STATE_CONNECTED, NULL);
		}
		else if (strcmp(msg->text_message, "GET_VER") == 0) {
			if (FW_CheckDirection(A_TO_B, msg->direction))
				FW_SwitchState(FW_STATE_WAITING_VER, FW_ValidateVer);
		}
		else if (strcmp(msg->text_message, "GET_DATA") == 0) {
			if (FW_CheckDirection(A_TO_B, msg->direction))
				FW_SwitchState(FW_STATE_WAITING_DATA, FW_ValidateData);
		}
		else if (strcmp(msg->text_message, "GET_FILE") == 0) {
			if (FW_CheckDirection(A_TO_B, msg->direction))
				FW_SwitchState(FW_STATE_WAITING_DATA, FW_ValidateFile);
		}
		else if (strcmp(msg->text_message, "GET_COMMAND") == 0) {
			if (FW_CheckDirection(A_TO_B, msg->direction))
				FW_SwitchState(FW_STATE_WAITING_DATA, FW_ValidateCommand);
		}
		else if (strcmp(msg->text_message, "GET_B64") == 0) {
			if (FW_CheckDirection(A_TO_B, msg->direction))
				FW_SwitchState(FW_STATE_WAITING_B64_DATA, FW_ValidateB64);
		}
		else if (strcmp(msg->text_message, "DISCONNECT") == 0) {
			if (FW_CheckDirection(A_TO_B, msg->direction))
				FW_SwitchState(FW_STATE_DISCONNECTING, NULL);
		}
		else if (strcmp(msg->text_message, "DISCONNECT_OK") == 0) {
			if (FW_CheckDirection(B_TO_A, msg->direction))
				FW_SwitchState(FW_STATE_INIT, NULL);
		}
		else
			FW_DropToFallbackState();
	}
	else {
		if (FW_CheckDirection(B_TO_A, msg->direction))
			FW_ProcessArbitraryData(msg->text_message);
	}
}

/* FUNCTION:  validate_message
 * 
 * PURPOSE:  
 *    This function is called for each SPLPv1 message between client 
 *    and server
 * 
 * PARAMETERS:
 *    msg - pointer to a structure which stores information about 
 *    message
 * 
 * RETURN VALUE:
 *    MESSAGE_VALID if the message is correct 
 *    MESSAGE_INVALID if the message is incorrect or out of protocol 
 *    state
 */
enum test_status validate_message(struct Message* msg) {
	FW_ProcessMessage(msg);
	if (FW_LastTransitionResult == FW_TRANSITION_SUCCESS)
		return MESSAGE_VALID;
	return MESSAGE_INVALID;
}
